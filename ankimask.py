from xml.dom import minidom
import sys
import subprocess
import uuid
import argparse

parser = argparse.ArgumentParser(
    prog = 'AnkiMask',
    description = 'Build an Anki deck of Image Occlusion Enhanced cards from a png background and a svg mask'
    )

parser.add_argument('-b','--background',required=True,help='PNG image file of the background')
parser.add_argument('-m','--mask',required=True,help='SVG file containing rectangles that mask answers')
parser.add_argument('-d','--deck-name',required=True,help='Deck name (can use :: to create subdeck tree)')
parser.add_argument('-o','--output-file',default='deck.txt')
parser.add_argument('-P','--profile',default='Utilisateur 1')
parser.add_argument('-p','--media-collection-path',default='~/.local/share/Anki2')
parser.add_argument('-H','--header',default='')
parser.add_argument('-F','--footer',default='')
args = parser.parse_args()
imageName = args.background
header = args.header
footer = args.footer
#Prepare txt file (in fact a simple csv file!)

deckName = args.deck_name

deckTxt = open(args.output_file,'a')

deckTxt.write('#separator:comma\n')
deckTxt.write('#html:true\n')
deckTxt.write('#guid column:1\n')
deckTxt.write('#notetype column:2\n')
deckTxt.write('#deck column:3\n')
deckTxt.write('#tags column:15\n')

# read global mask
globalMask = minidom.parse(args.mask)

# change rect ids to incremental id
qid = 1
for rect in globalMask.getElementsByTagName('rect'):
  rect.setAttribute('id',str(qid))
  qid += 1

nbOfQuestions = qid-1

qid = 1
# Question: for each rect, clone the dom and create a question with gray fill
# Answer: for each rect clone the dom and remove the rect   
for rect in globalMask.getElementsByTagName('rect'):
  cardMask = str(uuid.uuid4())

  questionMask = globalMask.cloneNode(True)
  for questionRect in questionMask.getElementsByTagName('rect'):
    if questionRect.getAttribute('id') == rect.getAttribute('id'):
      questionRect.setAttribute('style',questionRect.getAttribute('style').replace('ffffff','aaaaaa'))
      break
  questionName = imageName+'_q'+rect.getAttribute('id')
  questionMask.writexml(open(cardMask+'_q.svg','w'))
  
  answerMask = globalMask.cloneNode(True)
  for answerRect in answerMask.getElementsByTagName('rect'):
    if answerRect.getAttribute('id') == rect.getAttribute('id'):
      answerRect.parentNode.removeChild(answerRect)
      break
  answerName = imageName+'_a'+rect.getAttribute('id')
  answerMask.writexml(open(cardMask+'_a.svg','w'))

  # AnkiDroid does not handle svg media files. Convert to png with inkscape 
  subprocess.run(['inkscape',cardMask+'_q.svg','-o',cardMask+'_q.png'])
  subprocess.run(['inkscape',cardMask+'_a.svg','-o',cardMask+'_a.png'])

  deckTxt.write(cardMask+',Image Occlusion Enhanced,'+deckName+','+str(qid)+','+header+',"<img src=""'+imageName+'"">","<img src=""'+cardMask+'_q.png"">",'+footer+',,,,,"<img src=""'+cardMask+'_a.png"">",,\n')
  qid += 1

