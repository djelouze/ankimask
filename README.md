# Ankimask - Generation de cartes Anki à partir d'une image et d'un masque

Ce script génère des cartes type "Image Occlusion Enhanced" à partir d'un fond `PNG` et d'un masque `SVG`

## Pre-requis

- Python3
- Inkscape
- Anki

## Création du fond

Les cartes afficheront toujours le même fond. Il suffit donc d'extraire le schéma à réviser à l'aide, par exemple, d'une capture d'écran.
Firefox permet d'afficher un fichier pdf et de faire une capture d'écran:
```
Exemple:
- ouvrir exemple/document.pdf avec firefox
- clic droit -> effectuer une capture d'écran
- selectionner la zone voulue
- sauvegarder la capture d'écran en png
```

Le fichier `exemple/os_crane.png` a été créé de cette manière

## Création du masque

Ici inkscape sera utilisé pour masquer *tous* les textes de l'image à mémoriser

```
Exemple:
- ouvrir exemple/os_crane.png avec Inkscape
- dessiner un rectangle recouvrant chaque texte. Un rectangle générera une carte
- selectionner le fond (clic sur le schéma en dehors d'un rectangle) et supprimer
- selectionner tous les rectangles (ctrl+a devrait suffire : il n'y a plus que les rectangles dans l'image)
- depuis l'outil "fond et contour", selectionner un fond blanc (#ffffff) et pas de contour
- la page semble blanche, mais les rectangles sont bien toujours là
- sauvegarder la page en svg
```

Le fichier `exemple/os_crane_mask.svg` a été créé de cette manière

## Génération des cartes

En ligne de commande, se placer dans le répertoire contenant le fond et le masque puis executer:
```
$> python ankimask.py -b os_crane.png -m os_crane_mask.svg -d Exemple::Crane
```

(En ajoutant `-h`, vous afficherez les commandes possibles.)
```
usage: AnkiMask [-h] -b BACKGROUND -m MASK -d DECK_NAME [-o OUTPUT_FILE] [-P PROFILE] [-p MEDIA_COLLECTION_PATH] [-H HEADER] [-F FOOTER]

Build an Anki deck of Image Occlusion Enhanced cards from a png background and a svg mask

options:
  -h, --help            show this help message and exit
  -b BACKGROUND, --background BACKGROUND
                        PNG image file of the background
  -m MASK, --mask MASK  SVG file containing rectangles that mask answers
  -d DECK_NAME, --deck-name DECK_NAME
                        Deck name (can use :: to create subdeck tree)
  -o OUTPUT_FILE, --output-file OUTPUT_FILE
  -P PROFILE, --profile PROFILE
  -p MEDIA_COLLECTION_PATH, --media-collection-path MEDIA_COLLECTION_PATH
  -H HEADER, --header HEADER
  -F FOOTER, --footer FOOTER
```

Cette commande va générer un fichier svg et png par carte, en modifiant le masque original. Chaque rectangle deviendra tour à tour gris avec un svg/png correspondant à une question (finissant par `_q.png/svg`) puis sera supprimé avec un svg/png correspondant à une réponse (`_a.png/svg`)

Un fichier `deck.txt` est généré également, il contient les informations à importer

## Importation dans Anki

Pour l'instant, il faut copier "à la main" les médias générés (question/réponses png) dans le répertoire `collection.media` de votre profil anki:
```
$> cp *.png ~/.local/share/Anki2/Djelouze/collection.media
```

Puis ouvrir Anki, et dans l'onglet Paquets choisir `importer` et selectionner le deck.txt généré précédemment.

Vous aurez un nouveau deck appelé `Exemple` avec un sous-deck `Crane`

## Notes additionnelles:
- `Exemple::Crane` créé un paquet/sous-paquet. Vous pouvez générer un autre sous-paquet avec `Exemple::Thorax` qui sera ajouté au même paquet principal `Exemple`
- Si vous appelez plusieurs fois le script dans le même répertoire sans l'option `-o` ou avec le même nom de fichier de sortie, les cartes seront ajoutées au `deck.txt` existant. Utile pour créer des cartes avec plusieurs fond/masque mais en important qu'une seule fois.
- les options `-P` et `-p` sera utilisé pour copier automatiquement les fichiers dans `collection.media`
